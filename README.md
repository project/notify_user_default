CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Notify User Default module provides automatically checks off the
`$form['account']['notify']['#default_value']` to be `TRUE`
when creating new user accounts.
(URL "https://www.drupal.org/project/notify_user_default").
This module is currently available for Drupal 8.x.x. || 9.x.x

 * For the description of the module visit:
   https://www.drupal.org/project/notify_user_default

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/notify_user_default?categories=All


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Notify User Default module as you would normally
install a contributed Drupal module.
Visit https://www.drupal.org/project/notify_user_default for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    The user notify boolean field has now been updated.


MAINTAINERS
-----------

 * Ethan Aho- https://www.drupal.org/u/eahonet
 * @audetcameron - https://www.drupal.org/u/audetcameron

Supporting organization:

 * CommonPlaces Interactive - https://www.drupal.org/commonplaces-interactive
